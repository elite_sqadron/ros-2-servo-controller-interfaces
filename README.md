ROS2 package containing interfaces that are prerequisite for my other packages that deal with servo control.

## Install
1. Navigate to (or create) src subfolder of your ROS2 workspace (`cd ros2_ws/src`)
2. `git pull https://gitlab.com/elite_sqadron/ros-2-servo-controller-interfaces.git`
3. navigate one level up (`cd ../`)
4. build package with colcon (`colcon build --packages-select servo_controller_interfaces`)
